// Note, this js needs to be invoked with 'callAsyncJavaScript', because a delay is needed (sleep 2 sec) to be sure all dynamic page content is loaded and the app needs to wait for it
var p = new Promise(async function (resolve, reject) {

    sleep(2000);

    let r = {};

    // Kredietgegevens
    Debts = [];

    let creditItems = document.querySelectorAll('#all > ul > li');

    for (i = 0; i < creditItems.length; i++) {

        // Status
        let status = getAttribute('a > div > div.col-sm-12.col-md-2.pt-2 > span', null, creditItems[i]);

        // only store when debt is still active
        if (status != 'Lopend')
            continue;

        let item = {};

        let url = creditItems[i].querySelector('a').href;

        // get item
        let htmlString = httpGet(url);
        let dom = new DOMParser().parseFromString(htmlString, "text/html");

        // Aanbieder
        item.Supplier = getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(1) > div:nth-child(1) > span.font18', null, dom).trim();

        // Soort krediet
        item.Type = getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(1) > div:nth-child(4) > span.font18', null, dom).trim();

        // Details:

        // Contractnummer
        item.Contractnumber = getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(1) > div:nth-child(3) > span.font18', null, dom).trim();

        // Bedrag
        item.Amount = getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(1) > div:nth-child(2) > span.font18', null, dom).replace('€', '').trim().replace('.', '');

        // Registratiedatum
        item.RegistrationDate = formatDate(getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(4) > div:nth-child(1) > span.font18', null, dom));

        // Datum 1e aflossing
        item.FirstPaymentDate = formatDate(getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(4) > div:nth-child(2) > span.font18', null, dom));

        // Verwachte einddatum
        item.ExpectedEndDate = formatDate(getAttribute('body > div > div.content > div > div > div.col-12 > div > div.col-sm-12.col-md-8 > div:nth-child(1) > div.card-body > div:nth-child(4) > div:nth-child(3) > span.font18', null, dom));

        Debts.push(item);
    }

    r.Debts = JSON.stringify(Debts);

    //console.log(r);

    resolve(r);

});

await p;
return p;

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}

function formatDate(dateStr) {
    if (!dateStr || dateStr.trim() == '' || !dateStr.includes('-'))
        return '';

    let dobArr = dateStr.trim().split('-');
    let dt = new Date(dobArr[2], (dobArr[1] - 1), dobArr[0]);
    const offset = dt.getTimezoneOffset();
    dt = new Date(dt.getTime() - (offset * 60 * 1000));
    return dt.toISOString().split('T')[0];
}

function httpGet(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.response;
}
