import 'package:bkr/model/translation_model.dart';

class TranslationTemplate {
  static Map<String, TranslationModel> translationCollection() => {
        'EN': TranslationModel(backButtonText: 'Back', loadingText: 'You are now going to the website of BKR to login', processingText: ' Processing'),
        'UK': TranslationModel(backButtonText: 'Back', loadingText: 'You are now going to the website of BKR to login', processingText: ' Processing'),
        'NL': TranslationModel(backButtonText: 'Terug', loadingText: 'Je gaat nu naar iDIN om bij BKR in te loggen', processingText: 'Verwerken'),
      };
}
