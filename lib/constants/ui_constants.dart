import 'package:flutter/material.dart';

class UIConstants {
  static const defaultFontFamily = 'Walsheim';
  static const accentColor = Color.fromRGBO(250, 74, 105, 1);
  static const primaryColor = Colors.white;
  static const paleLilac = Color.fromRGBO(226, 225, 233, 1);
  static const mediumGrey = Color(0xff7c859a);
  static const headerTitleText = Color.fromRGBO(49, 48, 49, 1);
  static const watermelon = Color.fromRGBO(250, 74, 105, 0.64);
}
