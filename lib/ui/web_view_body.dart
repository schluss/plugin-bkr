import 'dart:async';
import 'dart:convert';

import 'package:bkr/model/translation_model.dart';
import 'package:bkr/reused_widgets/wait_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';

Logger _logger = Logger();
Locale? _locale;
late BuildContext ctx;
bool isShown = false;
late bool isTestEnvironment;
Map<String, dynamic> attributes = <String, dynamic>{};

class WebViewBody extends StatelessWidget {
  final Function webViewCreatedCallback; // use to call after webview created
  final String? initialUrl; // initial url to open
  final Function urlChangedCallback; // use to call after webview created
  final Function mainAppCallBack; // main application callback function
  final Locale locale;
  final StreamController<int> _streamController;

  void showWaiter(BuildContext context, String? text) async {
    if (!isShown) {
      WaitOverlay.show(context, _locale, text: text);
      isShown = true;
    }
  }

  void hideWaiter() {
    if (isShown) {
      WaitOverlay.hide();
      isShown = false;
    }
  }

  WebViewBody(
    this.webViewCreatedCallback,
    this.initialUrl,
    this.urlChangedCallback,
    this.mainAppCallBack,
    this._streamController,
    this.locale, {
    Key? key,
  }) {
    _locale = locale;

    // to keep track of whether we run this plugin on the test or real BKR environment
    isTestEnvironment = initialUrl!.contains('services.schluss.app');
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    @override
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    //AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    return Container(
      height: height * 0.9,
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InAppWebView(
                //initialUrlRequest: URLRequest(url: Uri.parse(initialUrl)),
                initialUrlRequest: URLRequest(
                  url: Uri.parse(initialUrl!),
                ),
                initialOptions: InAppWebViewGroupOptions(
                    android: AndroidInAppWebViewOptions(useHybridComposition: true),
                    crossPlatform: InAppWebViewOptions(
                        cacheEnabled:
                            false, //chnage to true or false to enable or disable cache
                        clearCache: true, //Clears all the webview's cache.
                        userAgent:
                            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36')),
                onWebViewCreated:
                    (InAppWebViewController appWebViewController) async {
                  webViewCreatedCallback(
                      appWebViewController); //call given function after webview created
                },
                onLoadStart: (controller, url) async {
                  var location = url.toString().toLowerCase();

                  if (isTestEnvironment) {
                    showWaiter(
                        context,
                        TranslationModel.getTranslation(locale)!
                            .processingText);
                  }

                  // idin (ideal) can contain many urls, because of many different banks used, we reverse the proces: show the waiter only at known pages
                  if (location == 'https://mijnkredietoverzicht.bkr.nl/inloggen/idin/' ||
                      location == 'https://mijnkredietoverzicht.bkr.nl/' ||
                      location ==
                          'https://mijnkredietoverzicht.bkr.nl/mijn-gegevens/' ||
                      location ==
                          'https://mijnkredietoverzicht.bkr.nl/overige-gegevens/') {
                    showWaiter(
                        context,
                        TranslationModel.getTranslation(locale)!
                            .processingText);
                  }
                },
                onLoadStop: (InAppWebViewController controller, url) async {
                  var location = url.toString().toLowerCase();

                  _logger.i('Plugin BKR - loaded url: ' + location);

                  // TEST ENVIRONMENT
                  if (isTestEnvironment) {
                    // 1. select bank (& login)
                    if (location.contains('index.html')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > div.login-page > div > div > div.first-block.col-lg-5.col-md-6.col-sm-12').style.display='none';
                      document.querySelector('body > div.container').style.display='none';
                      document.querySelector('body > footer').style.display='none';

                      document.getElementById('aSelectedIssuer').addEventListener('change', function(){
                        this.disabled = true;
                        document.body.appendChild('<div style="position:fixed;left: 50%;top: 50%; transform: translateX(-50%, -50%);width:50;height:50;z-index:9999">
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto;  display: block;" width="32px" height="32px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                          <circle cx="50" cy="50" fill="none" stroke="#aaaaaa" stroke-width="15" r="40" stroke-dasharray="121 75">
                            <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.7027027027027026s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
                          </circle>
                          </svg>
                          </div>');
                      });

                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 1: request to enter personal details
                    else if (location
                        .contains('intro1-inzage-pers-gegevens.html')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 2: request to enter address details
                    else if (location
                        .contains('intro2-inzage-adres-gegevens.html')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 3: confirm entered details
                    else if (location
                        .contains('intro3-inzage-controle-gegevens.html')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // BKR landingspage after login: Kredietoverzicht
                    else if (location.contains('mijnkredietoverzicht.html')) {
                      // parse the data
                      await parseMijnBKR(controller, 'creditParser.js',
                          asyncCall: true);

                      // go to the next page
                      await controller.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  'https://services.schluss.app/bkr/test/profiel.html')));
                    }
                    // Profiel page: information about personal details
                    else if (location.contains('profiel.html')) {
                      // parse the data
                      await parseMijnBKR(controller, 'mijnGegevensParser.js');

                      // Done
                      hideWaiter();

                      // logout
                      //await triggerLogout(location, controller);

                      // done, return
                      mainAppCallBack(jsonEncode(attributes), context);
                    } else {
                      hideWaiter();
                    }
                  }

                  // LIVE ENVIRONMENT
                  else {
                    // choose bank, to start idin login process
                    if (location
                        .contains('mijnkredietoverzicht.bkr.nl/inloggen')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > div.login-page > div > div > div.first-block.col-lg-5.col-md-6.col-sm-12').style.display='none';
                      document.querySelector('body > div.container').style.display='none';
                      document.querySelector('body > footer').style.display='none';

                      document.getElementById('SelectedIssuer').addEventListener('change', function(){
                        this.disabled = true;
                        document.body.appendChild('<div style="position:fixed;left: 50%;top: 50%; transform: translateX(-50%, -50%);width:50;height:50;z-index:9999">
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto;  display: block;" width="32px" height="32px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                          <circle cx="50" cy="50" fill="none" stroke="#aaaaaa" stroke-width="15" r="40" stroke-dasharray="121 75">
                            <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1.7027027027027026s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
                          </circle>
                          </svg>
                          </div>');
                      });                      
                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 1: request to enter personal details
                    else if (location.contains(
                        'mijnkredietoverzicht.bkr.nl/inzageverzoek/persoonlijke-gegevens')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 2: request to enter address details
                    else if (location.contains(
                        'mijnkredietoverzicht.bkr.nl/inzageverzoek/adresgegevens')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // first login: intro 3: confirm entered details
                    else if (location.contains(
                        'mijnkredietoverzicht.bkr.nl/inzageverzoek/bevestigen')) {
                      // cleanup the UI a little...
                      await controller.evaluateJavascript(source: '''
                      (function(){
                      document.querySelector('#sectionsNav').style.display='none';
                      document.querySelector('body > footer').style.display='none';
                      })();
                      ''');

                      hideWaiter();
                    }
                    // BKR landingspage after login: Kredietoverzicht
                    else if (location ==
                            'https://mijnkredietoverzicht.bkr.nl/' ||
                        location == 'https://mijnkredietoverzicht.bkr.nl') {
                      // parse the data
                      await parseMijnBKR(controller, 'creditParser.js',
                          asyncCall: true);

                      // go to the next page
                      await controller.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  'https://mijnkredietoverzicht.bkr.nl/profiel')));
                    }
                    // Profiel page: information about personal details
                    else if (location.contains('profiel')) {
                      // parse the data
                      await parseMijnBKR(controller, 'mijnGegevensParser.js');

                      // Done
                      hideWaiter();

                      // logout
                      //await triggerLogout(location, controller);

                      // done, return
                      mainAppCallBack(jsonEncode(attributes), context);
                    } else {
                      hideWaiter();
                    }
                  }
                }),
          )
        ],
      ),
    );
  }

  // Callback function for close and back buttons
  void closeOrBack() {
    hideWaiter(); // hide waiting screen
    Navigator.pop(ctx); // call previous UI
  }
}

//call when you need to perform logout
Future<void> triggerLogout(
    String url, InAppWebViewController inAppWebViewController) async {
  if (url == '#TODO') {
    String js;
    js =
        'window.setTimeout(function(){document.querySelector("#LoginStatus > dd > a").click()},0);';
    await inAppWebViewController.evaluateJavascript(source: js);
    await inAppWebViewController.clearCache(); // clear all cache

  }
}

Future parseMijnBKR(InAppWebViewController controller, String parserFilename,
    {asyncCall = false}) async {
  // parse data

  var parsedData;

  if (asyncCall) {
    var tmpData = await controller.callAsyncJavaScript(
      functionBody: await DefaultAssetBundle.of(ctx)
          .loadString('packages/bkr/assets/js/' + parserFilename),
    );

    parsedData = tmpData?.value;
  } else {
    parsedData = await controller.evaluateJavascript(
      source: await DefaultAssetBundle.of(ctx)
          .loadString('packages/bkr/assets/js/' + parserFilename),
    );
  }

  if (parsedData == null) {
    return;
  }

  var tmpMap = Map<String, dynamic>.from(
      parsedData); // Explicit Cast to <String, dynamic>, otherwise IOS error

  // add to attributes global
  attributes.addAll(tmpMap);
}
