import 'package:bkr/model/translation_model.dart';
import 'package:flutter/material.dart';

import '../constants/ui_constants.dart';
import 'loading_indicator.dart';
import 'mini_header_template.dart';

Locale? _locale;

class WaitOverlay {
  static late BuildContext ctx;

  static void hide() {
    try {
      if (Navigator.of(ctx, rootNavigator: true).canPop()) {
        Navigator.of(ctx, rootNavigator: true).pop();
      } else {
        print('System will not be able to close the Screen. please Review the Navigator Flow.');
      }
    } catch (e) {
      print(e);
    }
  }

  static void show(BuildContext context, Locale? locale, {String? text}) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    text ??= TranslationModel.getTranslation(_locale)!.loadingText;

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (dialogContext) {
        ctx = dialogContext;
        return Dialog(
            insetPadding: EdgeInsets.zero,
            child: Scaffold(
              backgroundColor: Colors.white,
              body: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + height * 0.1,
                    ),
                    height: height,
                    width: width,
                    color: UIConstants.primaryColor,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25,
                      ),
                      child: LoadingIndicator(_locale, text: text),
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        color: UIConstants.paleLilac,
                        child: MiniHeaderTemplate(
                          locale: locale,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ));
      },
    );
  }
}
