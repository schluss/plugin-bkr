library bkr;

import 'dart:async';
import 'dart:convert';

import 'package:bkr/interface/common_plugin_dao.dart';
import 'package:bkr/ui/web_view_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PluginBKR implements CommonPluginDAO {
  final StreamController<int> _streamController = StreamController.broadcast();

  @override
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'BKR',
  }) {
    Locale locale;
    locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(
          callBack,
          siteUrl,
          trigger,
          locale,
          _streamController,
          pluginName: pluginName,
        ),
      ),
    );
  }

//Extract data from xml file
  @override
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(100); // temporary

    // here we recieved a json encoded string (filePath), to be converted back to a Map<String,dynamic> to convert them to a ResponseAttributeModel
    _streamController.add(60);
    //await Future.delayed(Duration(seconds: 5)); only for testing
    Map<String, dynamic>? attributes;
    attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
